<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/fontawesome/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/fontawesome.min.css" integrity="sha512-xX2rYBFJSj86W54Fyv1de80DWBq7zYLn2z0I9bIhQG+rxIF6XVJUpdGnsNHWRa6AvP89vtFupEPDP8eZAtu9qA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="assets/css/footer.css">

<footer class="bg-dark text-white pt-5 pb-4">
<div class="container text-center text-md-left">
  <div class="row text-center tex-md-left">
    <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
      <h5 class="text-uppercase mb-4 font-weight-bold text-warning">Mudi-Khana</h5>
      <p>Mudi-khana is a web-based grocery shop, which is created by bootstarp-5,laravel </p>
    </div>
    <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3 " style="text-align: left;">
      <h5 class="text-uppercase mb-4 font-weight-blod text-warning">Products</h5>
      <p> <a href="#" class="text-white" style="text-decoration: none;"> All products </a></p>
      <p> <a href="#" class="text-white" style="text-decoration: none;"> All products </a></p>
      <p> <a href="#" class="text-white" style="text-decoration: none;"> All products </a></p>
    </div>
    <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3" style="text-align: left;">
      <h5 class="text-uppercase mb-4 font-weight-blod text-warning">Contact us</h5>
      <p>
      <i class="fa-solid fa-house" ></i>Road 15, Sector 6,Uttara, Dhaka
      </p>
      <p><i class="fa-solid fa-envelope "></i>mail@mudikhana.com</p>
      <p><i class="fa-solid fa-phone "></i>+880122222345</p>
    </div>

    <hr class="mb-4">

    <div class="row align-items-center">
      <div class="col-md-7 col-lg-8">
        <p>Copyright @2022 All right reserved by:
          <a href="#" style="text-decoration: none;"><strong class="text-warning">Hamidul haque</strong></a>
        </p>
      </div>
      <div class="col-md-5 col-lg-4">
        <div class="text-center text-md-right">
          <ul class="list-unstyled list-inline">
            <li class="list-inline-item">
              <a href="https://fb.com/hottestboz" class="btn-floating btn-sm text-white" style="font-size: 23px;"><i class="fa-brands fa-facebook"></i></a>
            </li>
            <li class="list-inline-item">
              <a href="https://gitlab.com/hamidulhaque" class="btn-floating btn-sm text-white" style="font-size: 23px;"><i class="fa-brands fa-gitlab"></i></a>
            </li>
          </ul>
        </div>
      </div>
    </div>

  </div>
  <hr class="mb-4">
  <div class="row align-items-center">
  <div class="input-group mb-3 mt-4">
     <input type="text" class="form-control" placeholder="Enter email" aria-label="Recipient's username" aria-describedby="button-addon2"> 
     <button class="btn btn-warning border-rad" type="button" id="button-addon2">Subscribe
       
     </button>
     </div>

  </div>

</div>
</footer>